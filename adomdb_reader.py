#!/usr/bin/env python3

"""adomdb_reader.py: It reads ADOM DB abd displays some useful stats"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"
__requires__ = ['paramiko']

import argparse
import paramiko
from collections import defaultdict
from functools import reduce


parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-a', '--adom',action='store')
group.add_argument('-f', '--file',action='store')
args = parser.parse_args()

# FMG / FAZ configuration
adom = args.adom
server_IP = "10.109.20.12"
username = "admin"
password = ""
port = 22



if args.adom:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server_IP, port, username, password)
    stdin, stdout, stderr = ssh.exec_command("execute fmpolicy print-adom-database " + adom)
    output = stdout.read().decode().splitlines()

if args.file:
    with open(args.file, 'r') as f:
        output = f.read().splitlines()


f = lambda: defaultdict(f)

def getFromDict(dataDict, mapList):
    return reduce(lambda d, k: d[k], mapList, dataDict)

def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

def count(object):
    return str(len(config["vdom"]["FortiGate"][object]))
def list(object):
    print('\n ## Listing "config ' + object + '":\n')
    print('---------------   START   ---------------\n')
    for line in config["vdom"]["FortiGate"][object]:
        print(line)
    print('\n---------------    END    ---------------')


class Parser(object):

    def __init__(self):
        self.config_header = []
        self.section_dict = defaultdict(f)

    def parse_config(self, fields):  # Create a new section
        self.config_header.append(" ".join(fields))

    def parse_edit(self, line):  # Create a new header
        self.config_header.append(line[0])

    def parse_set(self, line):  # Key and values
        key = line[0]
        values = " ".join(line[1:])
        headers= self.config_header+[key]
        setInDict(self.section_dict,headers,values)

    def parse_next(self, line):  # Close the header
        self.config_header.pop()

    def parse_end(self, line):  # Close the section
        self.config_header.pop()

    def parse_output(self):
        for line in output:
            # Clean up the fields and remove unused lines.
            fields = line.replace('"', '').strip().split(" ")

            valid_fields= ["set","end","edit","config","next"]
            if fields[0] in valid_fields:
                method = fields[0]
                # fetch and call method
                getattr(Parser, "parse_" + method)(self, fields[1:])

        return self.section_dict

config = Parser().parse_output()

print('\n ## Displaying some stats\n')

print(count("firewall address") + " firewall addresses")
print(count("firewall address6") + " firewall addresses ipv6")
print(count("firewall addrgrp") + " firewall address groups")
print(count("firewall addrgrp6") + " firewall address groups ipv6")
print(count("firewall vip") + " firewall vips")
print(count("firewall vip6") + " firewall vips ipv6")
print(count("firewall vipgrp") + " firewall vip grops")
print(count("firewall vipgrp6") + " firewall vip groups ipv6")
print(count("firewall ippool") + " firewall ip pools")
print(count("firewall ippool6") + " firewall ip pools ipv6")
print(count("antivirus profile") + " antivirus profiles")
print(count("webfilter profile") + " webfilter profiles")


list("firewall address")
list("webfilter profile")
